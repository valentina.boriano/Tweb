function Quiz(questions) {
    this.a=0;
    this.b=0;
    this.c=0;
    this.d=0;
    this.questions = questions;
    this.questionIndex = 0;
}

function Question(text, choices) {
    this.text = text;
    this.choices = choices;
}


function populate() {
    if(quiz.questionIndex === quiz.questions.length) {
        var max= Math.max(quiz.a,quiz.b,quiz.c, quiz.d);
        showScores(max);
    }
    else {
        // show question
        var element = document.getElementById("question");
        element.innerHTML = quiz.questions[quiz.questionIndex].text;

        // show options
        var choices = quiz.questions[quiz.questionIndex].choices;
        for(var i = 0; i < choices.length; i++) {
            var element = document.getElementById("choice" + i);
            element.innerHTML = choices[i];
            guess("btn" + i);
        }

    }
}

function guess(id) {
    var button = document.getElementById(id);
    button.onclick = function() {
        if(id=="btn0") quiz.a++;
        if(id=="btn1") quiz.b++;
        if(id=="btn2") quiz.c++;
        if(id=="btn3") quiz.d++;
        quiz.questionIndex++;
        populate();
    }
}



function showScores(max) {
    var gameOverHTML = "<h1>Result</h1>";
    if(quiz.a==max){
        gameOverHTML += "<h2 id='score'> Il tuo viaggio ideale è al: <a href=mare.php> Mare </h2>";
    }
    else if(quiz.b==max){
        gameOverHTML += "<h2 id='score'> Il tuo viaggio ideale è in: <a href=city.php> Città </h2>";
    }
    else if(quiz.c==max){
        gameOverHTML += "<h2 id='score'> Il tuo viaggio ideale è in: <a href=italia.php> Italia </h2>";
    }
    else{
        gameOverHTML += "<h2 id='score'> Il tuo viaggio ideale è nell': <a href=2020Travels.php> Avventura </h2>";
    }
    var element = document.getElementById("quiz");
    element.innerHTML = gameOverHTML;
}

// create questions here
var questions = [
    new Question("Quale di queste attività ti piace di più in vacanza?", ["Nuotare", "Fare shopping","Visitare Musei", "Scoprire nuove culture"]),
    new Question("Qual è il tuo cibo preferito?", ["Pesce", "Street Food", "Pasta", "Mi piace tutto"]),
    new Question("Cosa preferisci?", ["Rilassarti con un libro", "Passeggiare in città","L'arte e la cultura", "Fare nuove esperienze"])
];

// create quiz
var quiz = new Quiz(questions);

// display quiz
populate();