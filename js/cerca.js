function acquisti(el) {
    var dep = el.dataset.dep;
    var dest = el.dataset.dest;
    var partd = el.dataset.partd;
    var part = el.dataset.part;
    
    $.ajax({
        type: 'POST',
        url: 'server.php',
        data: {
            'dep': dep,
            'dest': dest,
            'partd': partd,
            'part': part,
            'acquistati': true
        },
        success: (el) => {
            alert(el);
        }
    })
    
}

function addToFavourites(el) {
    
    var dep = el.dataset.dep;
    var dest = el.dataset.dest;
    var partd = el.dataset.partd;
    var part = el.dataset.part;
    
    $.ajax({
        type: 'POST',
        url: 'server.php',
        data: {
            'dep': dep,
            'dest': dest,
            'partd': partd,
            'part': part,
            'adding_favourite': true
        },
        success: (el) => {
            alert(el);
        }
    })
    
}


     
function cerca(){
        // using this page stop being refreshing 
        event.preventDefault();

        var partenza = document.getElementById("partenza").value;
        var arrivo = document.getElementById("arrivo").value;
        var dataar = document.getElementById("dataa").value;
        var datapar = document.getElementById("datap").value;

        if(dataar < datapar) 
            alert("selezionare data di partenza prima della data di arrivo");
        else if(partenza == arrivo)
            alert("selezionare due città diverse!");
        else{
          $.ajax({
              type: 'POST',
              url: 'cercavoli.php',
              data: {
                  'partenza': partenza,
                  'arrivo': arrivo,
                  'dataar': dataar,
                  'datapar': datapar
              },
              success: function (msg) {
                  $("#voli").html(msg);
              }
          });   
        }
        
    }