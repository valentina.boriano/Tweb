<!DOCTYPE html>
<html>
    <head>
        <link href="/Tweb/css/cerca.css" type="text/css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="/Tweb/js/cerca.js"></script>

    </head>

    <body> 
        <?php include("../html/top.html"); ?>

        <form id=form>
            <div class='partenza'>
                 Aeroporto di partenza<br>
                <select name="partenza" id="partenza">
                    <option value="Bali">Bali</option>
                    <option value="Berlino">Berlino</option>
                    <option value="Buenos Aires">Buenos Aires</option>
                    <option value="Firenze">Firenze</option>
                    <option value="Londra">Londra</option>
                    <option value="Madrid">Madrid</option>
                    <option value="Maldive">Maldive</option>
                    <option value="Napoli">Napoli</option>
                    <option value="New york"> New York</option>
                    <option value="Oslo">Oslo</option>
                    <option value="Reykjavik">Reykjavik</option>
                    <option value="Roma">Roma</option>
                    <option value="Rottnest">Rottnest Island</option>
                    <option value="Sharm el-sheikh">Sharm el-sheikh</option>
                    <option value="Tokyo">Tokyo</option>
                    <option value="Torino">Torino</option>
                    <option value="Venezia">Venezia</option>

                </select>
            </div>

            <div class="datap">
                Data di partenza<br>
                <input type="date" id="datap" name="datap" value="2020-02-07"
               min="2020-02-07" max="2020-12-31"> <br>
            </div>

            <br><br>

             <div class='arrivo'>
               Aeroporto di arrivo<br> 
                <select name="arrivo" id="arrivo">
                    <option value="Bali">Bali</option>
                    <option value="Berlino">Berlino</option>
                    <option value="Buenos Aires">Buenos Aires</option>
                    <option value="Firenze">Firenze</option>
                    <option value="Londra">Londra</option>
                    <option value="Madrid">Madrid</option>
                    <option value="Maldive">Maldive</option>
                    <option value="Napoli">Napoli</option>
                    <option value="New york"> New York</option>
                    <option value="Oslo">Oslo</option>
                    <option value="Reykjavik">Reykjavik</option>
                    <option value="Roma">Roma</option>
                    <option value="Rottnest Island">Rottnest Island</option>
                    <option value="Sharm el-sheikh">Sharm el-sheikh</option>
                    <option value="Tokyo">Tokyo</option>
                    <option value="Torino">Torino</option>
                    <option value="Venezia">Venezia</option>

                </select><br>
            </div> 

           <div class="datar">
                Data del ritorno<br>
                <input type="date" id="dataa" name="dataa"
               value="datap"
               min="2020-02-07" max="2020-12-31">
           </div>
           <br>
           <div class="submit"><input type="submit" onclick="cerca()" value="cerca"></div>
        </form>
        
        <span id="voli"><p> Nessun Volo Selezionato</p></span>
   
        
        
      </body>
</html>