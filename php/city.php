<!DOCTYPE html>
<html>
    <head>
        <link href="/Tweb/css/city.css" type="text/css" rel="stylesheet">
    </head>

    <body> 
        <?php include("../html/top.html"); ?>
        
        <div id="city"><img class= "city" src="/Tweb/img/london.jpg" alt="Londra">
        
        <h1>City Breaks</h1>
        </div>
        
        <h2>Scopri le destinazioni Europee più belle</h2>
        
        <h3>Fly Air offre numerosissime destinazioni per moltissime città europee. Non farti scappare i nostri voli migliori. </h3>
        
        <h4> Madrid </h4>
        
        <img class= "madrid" src="/Tweb/img/madrid.jpg" alt="Madrid"> 
        
        <p>Tra i luoghi da visitare ricordiamo il Museo del Prado, il Museo Thyssen-Bornemisza, il Centro d' arte della regina Sofía (dove si trova il famoso Guernica di Pablo Picasso), il Palacio Real, la Puerta del Sol, in cui è situata la placca del chilometro zero (utilizzata per il calcolo delle distanze nello Stato spagnolo) ed in cui i madrileni si riuniscono ogni fine anno per aspettare il nuovo anno mangiando un chicco di uva per ognuno dei dodici rintocchi della mezzanotte, la vicina Plaza Mayor (in cui tutti i week-end si svolge il mercatino numismatico e, nel periodo natalizio, quello dei presepi). 
        <br> <br>
        Inoltre vi è anche il Parco del Retiro, e nelle vicinanze il Monastero dell'Escorial, Santa Cruz del Valle de los Caídos e le città di Toledo, Segovia, Ávila e Aranjuez. Da segnalare, soprattutto ai turisti italiani, la piccola chiesa dedicata a san Nicola di Bari, nei pressi di Plaza Mayor.
    </p>
        
        <button onclick= "location.href= 'cerca.php'"  id="madrid">Prenota Ora</button>
        
        <h4> Berlino </h4>
        
        <img class= "berlino" src="/Tweb/img/berlino.jpg" alt="Berlino"> 
        
        <p>Berlino è una città che offre musei di diverso genere.

        Il nome "Isola dei musei" è dovuto al gran numero di musei, di importanza internazionale, che si trovano nella parte settentrionale dell'Isola della Sprea. I musei sono parte del gruppo dei Musei statali di Berlino, appartenenti alla Fondazione culturale prussiana (Stiftung Preußischer Kulturbesitz).
        
        <br> <br>
         
        L'Altes Museum è il più antico dei musei, il Neues Museum espone tra i reperti dell'Arte Egizia anche il celebre busto di Nefertiti. Nell'Alte Nationalgalerie sono custodite opere d'arte del XIX secolo, mentre nel Bode-Museum, con la sua caratteristica cupola in rame, sono esposte opere romane e bizantine. Infine il Pergamon Museum famoso per ospitare l'Altare di Pergamo, la Porta di Ishtar babilonese, la porta del mercato di Mileto, le originali mura del palazzo omayyade della Mshatta, e in genere ricche collezioni di arte greca, babilonese e islamica.

        <br> <br>
            
       Il complesso del Kulturforum invece fu realizzato a partire dagli anni cinquanta del XX secolo, allo scopo di creare un centro culturale per l'allora Berlino Ovest, analogamente alla Museumsinsel per Berlino Est. Vi sorgono Neue Nationalgalerie,>Musikinstrumenten-Museum (museo degli strumenti musicali,Kunstgewerbemuseum e Gemäldegalerie.
        </p>
        
        <button onclick= "location.href= 'cerca.php'"  id="berlino">Prenota Ora</button>

        
        <h4> Oslo </h4>
        
        <img class= "oslo" src="/Tweb/img/oslo.jpg" alt="Oslo"> 
        
        <p>Oslo ospita molteplici musei tra cui il Museo Kon-Tiki dedicato alle avventure di Thor Heyerdahl (1914-2002), uno dei più famosi scienziati, avventurieri ed ambientalisti della storia, il Museo Kon-Tiki ospita le imbarcazioni originali e gli oggetti appartenuti alle sue spedizioni, famose in tutto il mondo.
            
        <br> <br>
        Esso vanta esibizioni permanenti su: Ra, Tigris, Fatu-Hiva, Kon-Tiki, e Isola di Pasqua. Ha un'area separata dedicata alle mostre temporanee, così come un tour di 30 metri all'interno di una grotta e un'esibizione sottomarina contenente la fedele riproduzione di uno squalo balena di 10 metri.
        <br> <br>
            
        Il più famoso museo di Oslo è il Museo Munch, che ospita varie opere di Edvard Munch, fra cui una delle quattro versioni de L'urlo.

        Il Museo delle tradizioni norvegesi è un museo all'aperto dedicato all'arte popolare e alla vita quotidina tradizionale. Al suo interno si può ammirare un'autentica stavkirke.

        Il Museo Vigeland si trova nel grande Frognerparken e si compone di 212 sculture all'aperto di Gustav Vigeland.
        </p>
        
        <button onclick= "location.href= 'cerca.php'"  id="oslo">Prenota Ora</button>
        
    </body>
</html>