<!DOCTYPE html>
<html>
    <head>
        <link href="/Tweb/css/2020travel.css" type="text/css" rel="stylesheet">
    </head>

    <body> 
        <?php include("../html/top.html"); ?>
        
        <div class="ita>"><img class= "ita" src="/Tweb/img/sezItalia.JPG" alt="Italia"></div>
        
        <h1>Le migliori destinazioni del 2020 con Fly Air</h1>
        
        <h2>Fly Air offre nuove destinazioni in tutto il mondo, vieni a scoprirle!</h2>
        
        <h3> Tokyo</h3>
        
        <img class= "tokyo" src="/Tweb/img/tokyo.jpg" alt="Tokyo"> 
        
        <p>Tokyo è sede di molti musei: il Centro nazionale delle Arti di Tokyo,il Museo nazionale d'arte occidentale,il Museo nazionale d'arte moderna, che contiene diverse collezioni di arte moderna giapponese e una sezione multimediale con oltre 40 000 titoli di film giapponesi ed esteri,il Museo nazionale delle scienze, anch'esso situato nel parco di Ueno. Nelle vicinanze sorge uno zoo pubblico,
        il Museo nazionale di Tokyo, situato nel parco di Ueno, è il più grande del Giappone e ospita la migliore collezione al mondo di arte giapponese, nonché un nutrito gruppo di opere d'arte asiatica.
        <br> <br>


        Tokyo ha molti teatri dove vengono rappresentati sia spettacoli contemporanei che le forme tradizionali di teatro giapponese, come il Nō e il Kabuki. Tali strutture vengono utilizzate anche per concerti di orchestre sinfoniche, che eseguono musica occidentale o anche musica tradizionale giapponese. Tokyo ospita anche concerti di musica pop e rock di artisti giapponesi e occidentali sia in club di piccole dimensioni generalmente chiamati live houses or stage houses, sia in strutture appositamente dedicate come il famoso e capiente palazzo dello sport del Nippon Budokan.</p>
        
        <button onclick= "location.href= 'cerca.php'" id="tokyo">Prenota Ora</button>
        
        <h3> Reykjavik </h3>
        
        <img class= "reykjavik" src="/Tweb/img/reykjavik.jpg" alt="Reykjavik"> 
        
        <p>Un edificio appariscente è il Perlan nel quartiere Öskjuhlið. Sotto una cupola di vetro si trova un enorme serbatoio di acqua bollente per il riscaldamento delle case, la fornitura di acqua calda e in inverno il riscaldamento delle strade e marciapiedi. Vi si trova pure un piccolo museo dedicato alle saghe islandesi, nonché alcuni negozi e, all'ultimo piano, un ristorante girevole che, compiendo una rotazione completa in due ore, offre ai clienti una vista completa sulla zona circostante.

        Importante la Dómkirkjan, il principale luogo di culto luterano della Chiesa nazionale d'Islanda.


        <br> <br>
        Un altro appariscente edificio, di recente costruzione e inaugurato nel maggio 2011, è l'Harpa. Situato di fronte all'incrocio principale del centro della città, incanta spesso i passanti con giochi di luci artificiali e riflessi del Sole sulle pareti esterne completamente vetrate.

        In generale la città è caratterizzata dalla presenza di tante piccole case con giardino. Le case sono solitamente rivestite in lamiera. In alcune zone, come ad esempio il porto, vengono costruiti edifici più grandi e di architettura moderna.
        </p>
        
        <button onclick= "location.href= 'cerca.php'" id="reykjavik">Prenota Ora</button>

        
        <h3> Rottnest Island </h3>
        
        <img class= "rottnestisland" src="/Tweb/img/rottnestisland.jpg" alt="Rottnest Island"> 
        
        <p>Con le sue 63 spiagge, Rottnest Island è una meta perfetta per gli amanti degli sport acquatici come il nuoto, l'immersione, il surf, la pesca e lo snorkeling. I bambini impazziranno di gioia al Just 4 Fun Aqua Park, a Thomson Bay, con percorsi su gonfiabili galleggianti, pareti d'arrampicata, percorso a ostacoli e paddle. Prendi l'autobus di Island Explorer per raggiungere le spiagge, potrai fare un abbonamento giornaliero, per il weekend o per la stagione. Esiste anche il Discovery Bus Tour che gira l'isola in 90 minuti mentr una guida dà informazioni sul patrimonio storico-culturale, sulla flora e la fauna del luogo. 
        <br> <br>
            
        Sono inoltre disponibili percorsi guidati e tour in Segway, che toccano gli insediamenti di Thomson Bay e le postazioni di artiglieria presenti sull'isola durante il secondo conflitto mondiale.
        </p>
        
        <button onclick= "location.href= 'cerca.php'" id="rottnestisland">Prenota Ora</button>
        
        <h3> New York </h3>
        
        <img class= "newyork" src="/Tweb/img/newyork.jpg" alt="New York"> 
        
        <p>New York è in una posizione di primo piano nel settore dello spettacolo statunitense: molti film, serie televisive, libri e altri media nascono qui. New York è il secondo più grande centro per l'industria cinematografica negli Stati Uniti,[88] e per volume, New York è il leader mondiale nella produzione cinematografica indipendente. La città conta più di 2.000 organizzazioni artistiche e culturali e oltre 500 gallerie d'arte di tutte le dimensioni.

        <br> <br>
            
        I fondi stanziati dall'amministrazione della città per le arti sono superiori rispetto al National Endowment for the Arts.[90] I ricchi industriali del XIX secolo hanno istituito una rete di importanti istituzioni culturali, come la famosa sala da concerto Carnegie Hall e il Metropolitan Museum of Art. L'avvento della luce elettrica ha portato a realizzare produzioni teatrali e nei teatri di New York, a Broadway e lungo la 42° strada, è iniziato un nuovo tipo di intrattenimento che divenne noto come il musical di Broadway. Fortemente influenzata dagli immigrati della città, produzioni come quelle di Edward Harrigan e George M. Cohan e altri hanno usato la musica per narrare i temi della speranza e ambizione. Tra i musical più di successo di tutti i tempi rappresentati a New York: Il Fantasma dell'Opera, Les Miserables, Cats e West Side Story.

        La città ha 39 grandi teatri (con più di 500 posti ciascuno) e sono conosciuti collettivamente come "Broadway".
        
        </p>
        
        <button onclick= "location.href= 'cerca.php'" id="newyork">Prenota Ora</button>
        
        <h3> Buenos Aires </h3>
        
        <img class= "buenosaires" src="/Tweb/img/buenosaires.jpg" alt="Buenos Aires"> 
        
        <p>La musica simbolo di Buenos Aires e della cultura argentina in tutto il mondo è il tango. Questo ritmo nasce su entrambe le coste del Río de la Plata (coste di Argentina e Uruguay) come ibrido di altre specie popolari come il candombe, la milonga, il tango andaluz o la habanera ed è circoscritto ai gruppi marginali della città. Divenuto tipico dei bordelli, viene inizialmente rifiutato dalle classi medie e alte e solo nel 1910, periodo di successo internazionale, il tango verrà accettato per poter poi divenire una moda nei grandi saloni delle capitali europee.


        <br> <br>
            
        Un importante contributo al tango è stato dato dagli immigrati italiani, che abitavano nella città sul finire dell'Ottocento e gli inizi del Novecento. Fra i figli di italiani si annoverano grandi nomi del tango come Aníbal Troilo, Juan Maglio ''Pacho'', Juan D'Arienzo, Carlos Di Sarli, Osvaldo Pugliese, Edgardo Donato, Pascual Contursi, Carlos César Lenzi, Juan Bautista Deambrogio. Lo stesso Astor Piazzolla, sebbene cresciuto a New York, aveva i nonni italiani, pugliesi da parte di padre, toscani da parte di madre.
        </p>
        
        <button onclick= "location.href= 'cerca.php'" id="buenosaires">Prenota Ora</button>
    </body>
</html>