<!DOCTYPE html>
<html>
    <head>
        <link href="/Tweb/css/italia.css" type="text/css" rel="stylesheet">
    </head>

    <body> 
        <?php include("../html/top.html"); ?>
        
        <div class="ita>"><img class= "ita" src="/Tweb/img/sezItalia.JPG" alt="Italia"></div>
        
        <h1>5 Destinazioni da Visitare con Fly Air in Italia</h1>
        
        <h2>Fly Air offre numerosissime destinazioni eccitanti per le vacanze se avete voglia di esplorare l'Italia. Date un'occhiata alla nostra lista delle 20 destinazioni da visitare in Italia. </h2>
        
        <h3> Roma </h3>
        
        <img class= "roma" src="/Tweb/img/Roma.jpg" alt="Roma"> 
        
        <p>Si tratta del comune più popoloso d'Italia e il quarto dell'Unione europea dopo Londra, Berlino e Madrid.
        <br> <br>
        Fondata secondo la tradizione il 21 aprile del 753 a.C., nel corso dei suoi tre millenni di storia è stata la prima metropoli dell'umanità,cuore pulsante di una delle più importanti civiltà antiche, che influenzò la società, la cultura, la lingua, la letteratura, l'arte, l'architettura, la filosofia, la religione, il diritto e i costumi dei secoli successivi. Luogo di origine della lingua latina.Per antonomasia, è definita l'Urbe, Caput Mundi e Città eterna.
        <br> <br>
        Il suo centro storico, delimitato dal perimetro delle mura aureliane, sovrapposizione di testimonianze di quasi tre millenni, è espressione del patrimonio storico, artistico e culturale del mondo occidentale europeo e, nel 1980, insieme alle proprietà extraterritoriali della Santa Sede nella città, è stato inserito nella lista dei Patrimoni dell'umanità dell'UNESCO.</p>
        
        <button onclick= "location.href= 'cerca.php'"  id="roma">Prenota Ora</button>
        
        <h3> Venezia </h3>
        
        <img class= "venezia" src="/Tweb/img/venezia.jpg" alt="Venezia"> 
        
        <p>Venezia è una città di 259 809 abitanti,il cui centro storico (limitato ai sestieri della città lagunare).
        <br> <br>
        La città è stata per 1100 anni la capitale della Serenissima Repubblica di Venezia ed è conosciuta a questo riguardo come la Serenissima, la Dominante e la Regina dell'Adriatico: per le peculiarità urbanistiche e per il suo patrimonio artistico, è universalmente considerata una tra le più belle città del mondo, dichiarata, assieme alla sua laguna, patrimonio dell'umanità dall'UNESCO.
        <br> <br>
            
        Il luogo più celebre della città è Piazza San Marco, l'unica nel centro storico ad essere caratterizzata dal toponimo "piazza": le altre piazze sono chiamate infatti "campi" o "campielli". La Basilica di San Marco è situata al centro della piazza, colorata d'oro e rivestita da mosaici che raccontano la storia di Venezia, assieme ai bassorilievi che raffigurano i mesi dell'anno. 
        </p>
        
        <button onclick= "location.href= 'cerca.php'"  id="venezia">Prenota Ora</button>

        
        <h3> Torino </h3>
        
        <img class= "torino" src="/Tweb/img/torino.jpg" alt="Torino"> 
        
        <p>Torino fu la prima capitale del Regno d'Italia (dal 1861 al 1865).
        <br> <br>
            
        È stata la patria, natia o adottiva, di alcuni fra i più grandi scrittori e letterati italiani del XIX e XX secolo, tra i quali Edmondo De Amicis, Emilio Salgari, Italo Calvino, Natalia Ginzburg, Norberto Bobbio, Cesare Pavese e Primo Levi.
        <br> <br>
            
        Sede nel 2006 dei XX Giochi olimpici invernali, città natale di alcuni fra i maggiori simboli del Made in Italy nel mondo, come il Martini, il cioccolato gianduja e il caffè espresso, è il fulcro dell'industria automobilistica italiana, nonché importante centro dell'editoria, del sistema bancario e assicurativo, delle tecnologie dell'informazione, del cinema, dell'enogastronomia, del settore aerospaziale, del disegno industriale e dello sport. 
        </p>
        
        <button onclick= "location.href= 'cerca.php'"  id="torino">Prenota Ora</button>
        
        <h3> Napoli </h3>
        
        <img class= "napoli" src="/Tweb/img/napoli.jpg" alt="Napoli"> 
        
        <p>Città dall'imponente tradizione nel campo delle arti figurative, che affonda le proprie radici nell'età classica, ha dato luogo a movimenti architettonici e pittorici originali, quali il rinascimento napoletano e il barocco napoletano, il caravaggismo, la scuola di Posillipo ed il liberty napoletano, nonché ad arti minori ma di rilevanza internazionale, quali la porcellana di Capodimonte ed il presepe napoletano.

        <br> <br>
            
        È all'origine di una forma distintiva di teatro, di una canzone di fama mondiale e di una peculiare tradizione culinaria che comprende alimenti che assumono il ruolo di icone globali, come la pizza napoletana, e l'arte dei suoi pizzaioli che è stata dichiarata dall'UNESCO patrimonio immateriale dell'umanità.

        <br> <br>
        Nel 1995 il centro storico di Napoli è stato riconosciuto dall'UNESCO come patrimonio mondiale dell'umanità, per i suoi eccezionali monumenti, che testimoniano la successione di culture del Mediterraneo e dell'Europa. Nel 1997 l'apparato vulcanico Somma-Vesuvio è stato eletto dalla stessa agenzia internazionale (con il vicino Miglio d'Oro, in cui ricadono anche i quartieri orientali della città) tra le riserve mondiali della biosfera. 
        
        </p>
        
        <button onclick= "location.href= 'cerca.php'"  id="napoli">Prenota Ora</button>
        
        <h3> Firenze </h3>
        
        <img class= "firenze" src="/Tweb/img/firenze.jpg" alt="Firenze"> 
        
        <p>Firenze ha un importante centro universitario e patrimonio dell'umanità UNESCO dal 1982, è considerata luogo d'origine del Rinascimento – la consapevolezza di una nuova era moderna dopo il Medioevo, periodo di cambiamento e "rinascita" culturale e scientifica – e della lingua italiana grazie al volgare fiorentino usato nella letteratura. 
        <br> <br>
            
        È universalmente riconosciuta come una delle culle dell'arte e dell'architettura, nonché rinomata tra le più belle città del mondo, grazie ai suoi numerosi monumenti e musei tra cui il Duomo, Santa Croce, Santa Maria Novella, gli Uffizi, Ponte Vecchio, Piazza della Signoria, Palazzo Vecchio e Palazzo Pitti. Di inestimabile valore i lasciti artistici, letterari e scientifici di geni del passato come Petrarca, Boccaccio, Brunelleschi, Michelangelo, Giotto, Cimabue, Botticelli, Leonardo da Vinci, Donatello, Lorenzo de’ Medici, Machiavelli, Galileo Galilei e Dante Alighieri, che fanno del centro storico di Firenze uno dei luoghi con la più alta concentrazione di opere d'arte al mondo. 
        <br> <br>
        La ricchezza del patrimonio storico-artistico, scientifico, naturalistico e paesaggistico rendono il centro e le colline circostanti un vero e proprio "museo diffuso".
        </p>
        
        <button onclick= "location.href= 'cerca.php'"  id="firenze">Prenota Ora</button>
    </body>
</html>