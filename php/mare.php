<!DOCTYPE html>
<html>
    <head>
        <link href="/Tweb/css/mare.css" type="text/css" rel="stylesheet">
    </head>

    <body> 
        <?php include("../html/top.html"); ?>
        
        <div id="beach"><img class= "beach" src="/Tweb/img/beach.jpg" alt="Londra">
        
        <h1>Sole d'inverno</h1>
        </div>
        
        <h2>Scopri le destinazioni più calde per questo inverno</h2>
        
        <h3>Hai bisogno di una pausa estiva? Con Fly Air è possibile. </h3>
        
        <h4> Maldive </h4>
        
        <img class= "maldive" src="/Tweb/img/maldive.jpg" alt="Maldive"> 
        
        <p>Compongono l'arcipelago 1.192 isole coralline poggiate su basamenti di roccia calcarea e corallina, formatasi con evoluzioni periodiche e caratterizzata da molteplici strati di calcare e coralli formatesi con le numerose variazioni di livello delle acque ad iniziare da circa 60 milioni di anni fa, a seguito dell'emersione d'imponenti montagne dal fondo dell'Oceano Indiano. Gli atolli dell'arcipelago maldiviano sono in effetti tra le tracce più evidenti dell'attività vulcanica del punto caldo di Riunione, un punto caldo attualmente situato sotto l'isola di Riunione, nell'arcipelago delle isole Mascarene.

        <br> <br>
            
        Gli atolli naturali sono 26, ognuno è formato da diverse centinaia di isole, di cui solo alcune abitate. Nell'intero arcipelago, le isole abitate sono circa 200, mentre poco più di 100 sono adibite a villaggi turistici; le rimanenti sono deserte e talvolta costituite solo da un banco di sabbia in emersione. L'isola più grande è Fua Mulaku, situata nell'atollo di Gnaviyani, nel sud dell'arcipelago.
    </p>
        
        <button onclick= "location.href= 'cerca.php'"  id="maldive">Prenota Ora</button>
        
        <h4> Bali </h4>
        
        <img class= "bali" src="/Tweb/img/bali.jpg" alt="Bali"> 
        
        <p>A Bali sorgono le località di Kuta e Seminyak (a sud-ovest), di Sanur (a sud-est) e di Jimbaran e il nuovo insediamento di Nusa Dua (a sud). Seminyak è la parte più moderna e lussuosa, insieme a Nusa Dua. Kuta è la meta più ambita dai surfisti, mentre Ubud, al centro dell'isola, tra foreste e terrazze di riso, è senza dubbio la capitale culturale di Bali e vi si ritrova la serenità rurale e l'inimitabile spirito balinese. 
         <br> <br>
            
        La penisola meridionale di Bukit è la zona più arida dell'isola, ma vanta numerose spiagge nascoste e reef per i surfisti più coraggiosi. Il nord dell'isola è indicato per chi cerca pace e tranquillità, inoltre offre i migliori fondali marini da snorkeling. A est di Bali si trova Nusa Lembongan, tranquilla isola di coltivatori di alghe marine dove si respira ancora un'atmosfera ormai dimenticata.

        <br> <br>

        Grazie alla vita notturna della zona di Kuta, Bali è soprannominata da qualcuno l'Ibiza d'oriente. Vicino a Jimbaran, sull'istmo che collega la parte più meridionale dell'isola, sorge l'aeroporto internazionale Ngurah Rai.

        </p>
        
        <button onclick= "location.href= 'cerca.php'"  id="bali">Prenota Ora</button>

        
        <h4> Sharm el-Sheikh </h4>
        
        <img class= "sharm" src="/Tweb/img/sharm%20el%20sheikh.jpg" alt="Sharm el-Sheikh"> 
        
        <p>Sharm el-Sheikh è diventata una meta importante per la pratica della subacquea, con sub provenienti da tutto il mondo. Situata nel Mar Rosso offre scenari subacquei di grande bellezza anche grazie alla splendida barriera corallina e una temperatura dell'acqua piacevole per le immersioni. Le immersioni subacquee, lo snorkeling, il windsurf e il canottaggio sono comunemente praticati dai turisti.
            
        </p>
        
        <button onclick= "location.href= 'cerca.php'"  id="sharm">Prenota Ora</button>
        
    </body>
</html>