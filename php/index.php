<?php 
    include("../html/top.html");
	session_start(); 

	if (!isset($_SESSION['username'])) {
		$_SESSION['msg'] = "You must log in first";
		header('location: login.php');
	}

	if (isset($_GET['logout'])) {
		session_destroy();
		unset($_SESSION['username']);
		header("location: login.php");
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
        <link href="/Tweb/css/index.css" type="text/css" rel="stylesheet">

</head>
<body>

	<div class="content">

		<!-- notification message -->
		<?php if (isset($_SESSION['success'])) : ?>
			<div class="error success" >
				<h3>
					<?php 
						echo $_SESSION['success']; 
						unset($_SESSION['success']);
					?>
				</h3>
			</div>
		<?php endif ?>

		<!-- logged in user information -->
		<?php  if (isset($_SESSION['username'])) : ?>
			<p>Benvenuto <strong><?php echo $_SESSION['username']; ?></strong> (<a href="index.php?logout='1'">logout</a>)</p>
		<?php endif ?>
	</div>
		
        
         <h1>Lasciati Ispirare</h1>
        
        <div class="lasciatispirare">
            <div class= "londra"><a href= "city.php"> <img src="/Tweb/img/londra.jpg" alt="City "></a>
            <h2>City Breaks</h2>
            </div>
            
            <div class= "mare"><a href= "mare.php"> <img src="/Tweb/img/beach.jpg" alt="Mare"></a>
            <h3>Mare</h3> </div>
            
            <div class= "italia"><a href= "italia.php"> <img src="/Tweb/img/italia.jpg" alt="Italia"></a>
                <h4>Italia</h4></div>
            
            <div class= "travel"><a href= "2020travel.php"> <img src="/Tweb/img/fuji.jpg" alt="2020 Travel"></a>
                <h5>2020 Travels</h5> </div>
            
        </div>      
    </body>
</html>